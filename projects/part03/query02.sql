.read data.sql

delete from Likes as l1
where not exists(
    select *
    from Likes as l2
    where l1.ID1 = l2.ID2
    and l2.ID1 = l1.ID2)
and exists (
    select *
    from Friend
    where l1.ID1 = Friend.ID1
    or l1.ID1 = Friend.ID2)
and exists (
    select * 
    from Friend
    where (l1.ID1 = Friend.ID1
        and l1.ID2 = Friend.ID2)
    or (l1.ID1 = Friend.ID2
        and l1.ID2 = Friend.ID1)
);

