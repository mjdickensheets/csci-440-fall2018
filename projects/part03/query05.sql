.read data.sql

select name, grade
from highschooler as h1
where not exists(
    select *
    from friend
    inner join highschooler as h2
    on id2 = h2.id
    where h1.id = id1
    and h1.grade = h2.grade
)
and not exists(
    select *
    from friend
    inner join highschooler as h3
    on id1 = h3.id
    where h1.id = id2
    and h1.grade = h3.grade
)
;
