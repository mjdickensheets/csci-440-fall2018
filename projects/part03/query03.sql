.read data.sql

insert into Friend
select F1.ID1, F2.ID2 
from Friend 
as F1 
join Friend 
as F2 
where 
F1.ID2 = F2.ID1 and F1.ID1 != F2.ID2
union
select F1.ID2, F2.ID2
from Friend as F1
join Friend as F2
where
F1.ID1 = F2.ID1 and F1.ID2 != F2.ID2
union
select F1.ID1, F2.ID1
from Friend as F1
join Friend as F2
where
F1.ID2 = F2.ID2 and F1.ID1 != F2.ID1
except 
select * 
from Friend
except
select ID2, ID1
from Friend;
