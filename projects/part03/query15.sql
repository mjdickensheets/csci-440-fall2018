.read data.sql

select h1.name, h1.grade, h2.name, h2.grade, h3.name, h3.grade
from highschooler as h1
inner join likes
on h1.id = likes.id1
inner join highschooler as h2
on likes.id2 = h2.id
inner join friend
on h2.id = friend.id1
or h2.id = friend.id2
inner join highschooler as h3
on friend.id1 = h3.id
or friend.id2 = h3.id
where exists(
    select * 
    from friend as f1
    where (h1.id = f1.id1
        and h3.id = f1.id2)
    or (h1.id = f1.id2
        and h3.id = f1.id1)
)
and exists(
    select *
    from friend as f2
    where (h2.id = f2.id1
        and h3.id = f2.id2)
    or (h2.id = f2.id2
        and h3.id = f2.id1)
)
and not exists(
    select *
    from friend as f3
    where (h1.id = f3.id1
        and h2.id = f3.id2)
    or (h1.id = f3.id2
        and h2.id = f3.id1)
)
;
