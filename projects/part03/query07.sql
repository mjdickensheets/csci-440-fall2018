.read data.sql

select count(distinct h2.id) - 1
from highschooler as h1
inner join friend as f1
on h1.id = f1.id1
or h1.id = f1.id2
inner join friend as f2
on ((f2.id1 = f1.id1
            or f2.id2 = f1.id1)
        and h1.id != f1.id1)
    or ((f2.id1 = f1.id2
            or f2.id2 = f1.id2)
        and h1.id != f1.id2)
inner join highschooler as h2
on h2.id = f1.id1
or h2.id = f1.id2
or h2.id = f2.id1
or h2.id = f2.id2

where h1.name = "Cassandra"
and f2.id1 != h1.id
and f2.id2 != h1.id
;
