.read data.sql

select h1.name, h1.grade, h2.name, h2.grade 
from highschooler as h1 
inner join likes
on h1.id = id1
inner join highschooler as h2
on id2 = h2.id
where exists (
    select * 
    from likes as l1
    where h1.id = l1.id1 and h2.id = l1.id2)
and exists (
    select *
    from likes as l2
    where h2.id = l2.id1 and h1.id = l2.id2)
and h1.name < h2.name
;
