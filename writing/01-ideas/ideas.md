# Team Members

* Michael Dickensheets 
* Tim Osen

# Converting an ER diagram to a relation schema
## Objective
Instruct a reader in creating a proper relational schema from an ER diagram. Basically encapsulate the algorithm provided in the book in tutorial form. 
## Value
People will want to read this (assuming they have read a previous tutorial on constructing an ER diagram) in order to learn how to transform their ER diagram into something useful in an effective manner. It will also help the reader gain a better understanding of how relations are constructed within a database.

# Reducing relations and the normal forms
## Objective
Instruct a reader in the different types of normal forms, their purpose and the steps to take in order to ensure that a relational schema is in the desired normal form.
## Value
People will want to read this in order to better understand the concept of normal forms and be able to implement those concepts within their own database schemas. 

# Sqlite Basics
## Objective
Instruct a reader on the basic syntax and commands for the Sqlite system. Give users a base degree of comfort with Sqlite generally, but specifically with regard to datasets. Also provide a cheat-sheet/easy reference for new users. Include descriptions and examples with tables within the text to illustrate the functionality of the commands. 
## Value
Basic/entry level programming language descriptions are immensely helpful when getting started learning a new programming language. This guide would hopefully be a useful reference to both new students attempting to create their first queries as well as a good refresher for more experienced SQL users who need to remember the specifics of Sqlite. After completion of the tutorial readers will be familiar with basic syntax and commands within Sqlite with a focus on commands most commonly used and helpful while analyzing datasets.

